FROM alpine:3.9.4@sha256:769fddc7cc2f0a1c35abb2f91432e8beecf83916c421420e6a6da9f8975464b6 as V2ray

# 更新源、安装openssh 并修改配置文件和生成key 并且同步时间
RUN apk update && \
    apk add --no-cache openssh-server tzdata && \
    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    sed -i "s/#PermitRootLogin.*/PermitRootLogin yes/g" /etc/ssh/sshd_config && \
    ssh-keygen -t rsa -P "" -f /etc/ssh/ssh_host_rsa_key && \
    ssh-keygen -t ecdsa -P "" -f /etc/ssh/ssh_host_ecdsa_key && \
    ssh-keygen -t ed25519 -P "" -f /etc/ssh/ssh_host_ed25519_key && \
    echo "root:yuzc1234" | chpasswd
 
# 开放22端口
EXPOSE 22
 
# 执行ssh启动命令
CMD ["/usr/sbin/sshd", "-D"]

RUN mkdir /usr/bin/v2ray \
  && wget -q -O - https://github.com/v2ray/v2ray-core/releases/download/v4.18.2/v2ray-linux-64.zip \
  | unzip -d /usr/bin/v2ray - v2ray v2ctl \
  && chmod +x /usr/bin/v2ray/*

FROM alpine:3.9.4@sha256:769fddc7cc2f0a1c35abb2f91432e8beecf83916c421420e6a6da9f8975464b6

COPY --from=V2ray /usr/bin/v2ray /usr/bin/v2ray
ENV PATH=$PATH:/usr/bin/v2ray

COPY v2ray /v2ray
RUN chmod +x /v2ray/docker-entrypoint.sh

ENV DOCKER_ENV=true
ENTRYPOINT [ "/v2ray/docker-entrypoint.sh" ]

WORKDIR /v2ray

CMD [ "v2ray", "-config", "/v2ray/config.jsonc" ]
